# # encoding: utf-8

# Inspec test for recipe gitlab-mitigate-sackpanic::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

control 'basic-sackpanic-mitigation' do
  impact 1.0

  describe iptables do
    it { should have_rule('-A INPUT -p tcp -m tcpmss --mss 1:500 -j DROP') }
  end

  # Check that we've had a rule file saved appropriately.  A bit naff we have
  # to know the name of the file, but it's not a disaster
  # Don't bother with the precise contents; we trust that iptables-persistent
  # will have done it right
  describe file('/etc/iptables.d/filter/INPUT/00-mitigate-sackpanic.rule_v4') do
    it { should exist }
    its('type') { should eq :file }
    its('size') { should be > 0 }
  end

  # Annoyingly it's impossible to test locally that the rule is
  # being hit.  Even with something like scapy, any local traffic doesn't
  # hit the INPUT chain.  We'd need to send the packet from outside
  # which just gets hard and can't fit into inspec.  We'll just have
  # to trust that it's actually working
end
