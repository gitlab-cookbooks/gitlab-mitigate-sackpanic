# frozen_string_literal: true

name 'gitlab-mitigate-sackpanic'
maintainer 'GitLab Inc.'
maintainer_email 'ops-contact@gitlab.com'
license 'MIT'
description 'Adds mitigation for SACKPanic TCP bug in linux kernel'
long_description 'Adds mitigation for SACKPanic TCP bug in linux kernel'
version '0.1.5'
chef_version '>= 12.19' if respond_to?(:chef_version)

depends 'iptables-ng', '~> 4.0.0'
