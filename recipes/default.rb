# Cookbook:: gitlab-mitigate-sackpanic
# Recipe:: default
#
# Copyright:: 2019, GitLab Inc.

# Uninstalls ufw; this should be ok
include_recipe 'iptables-ng::install'

iptables_ng_rule '00-mitigate-sackpanic' do
  name      '00-mitigate-sackpanic'
  chain     'INPUT'
  table     'filter'
  ip_version 4
  rule       '-p tcp -m tcpmss --mss 1:500 -j DROP'
  action     :create
end
