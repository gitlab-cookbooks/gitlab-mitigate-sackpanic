#
# Cookbook:: gitlab-mitigate-sackpanic
# Spec:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

require 'spec_helper'

describe 'gitlab-mitigate-sackpanic::default' do
  context 'When all attributes are default, on Ubuntu 18.04' do
    let(:chef_run) do
      # for a complete list of available platforms and versions see:
      # https://github.com/customink/fauxhai/blob/master/PLATFORMS.md
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '18.04')
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates a single iptables rule dropping small MSS packets' do
      expect(chef_run).to create_iptables_ng_rule('00-mitigate-sackpanic')
        .with(
          name:      '00-mitigate-sackpanic',
          chain:     'INPUT',
          table:     'filter',
          ip_version: 4,
          rule:       '-p tcp -m tcpmss --mss 1:500 -j DROP',
          action:     [:create]
        )
    end
  end
end
